with open("feed.csv") as ni:
    data = ni.readlines()

headers = data.pop(0)
headers = headers.strip().split(",")
headers.append("Nitrate presence")

result = open("result.csv", "w")
result.write(",".join(headers)+"\n")
for i in data:
    line = i.strip().split(",")
    if float(line[2]) >= 6.5 and float(line[2]) <= 6.8:
        line.append("True")
    else:
        line.append("False")

    result.write(",".join(line)+"\n")    
    
