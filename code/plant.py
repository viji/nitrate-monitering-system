with open("feed2.csv") as ni:
    data = ni.readlines()

headers = data.pop(0)
headers = headers.strip().split(",")
headers.append("Plant_life_prediction")

result = open("result1.csv", "w")
result.write(",".join(headers)+"\n")
for i in data:
    line = i.strip().split(",")
    if float(line[2]) >= 6.5 and float(line[2]) <= 6.8 and int(line[3]) >35:
        line.append("Plant_Will_Die")
    else:
        line.append("Plant_in_Normal_condition")

    result.write(",".join(line)+"\n")    
    
