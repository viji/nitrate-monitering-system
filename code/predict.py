import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import Perceptron, LogisticRegression
from sklearn.preprocessing import StandardScaler

#Reading csv
plant_data = pd.read_csv("result1.csv", header=0)
maping = {'Plant_in_Normal_condition':3, 'Plant_Will_Die':1, 'Plant_under_risk':2}
plant_data['Plant_life_prediction'] = plant_data['Plant_life_prediction'].map(maping)

#Taking the result value
result = plant_data.iloc[:, -1].values

#Taking the factors value which leds to the result
dataset = plant_data.iloc[:, 2:4].values

#Splitting the data for train and test
dataset_train, dataset_test, result_train, result_test = train_test_split(dataset, result, test_size=0.3)

#Taking the date list for ploting in the graph
date_list = list(plant_data.iloc[:,0].values)
date_list = date_list[:len(list(dataset_test))]

#Initiating the algorithm
ppn = LogisticRegression(penalty='l1', C=1.0)
#Fitting the data into algorithm for learning
ppn.fit(dataset_train, result_train)
#Making the algorithm to find answer with test case
result_predect = ppn.predict(dataset_test)

#Saving the result into the csv file
np.savetxt("life_result.csv", result_predect, delimiter=",")
print(list(result_predect))

#Plotting the graph
result_list = list(result_predect)
ph_list = dataset_test[:,0]

plt.figure(1)
plt.subplot(211)
plt.title("Plant life graph")
plt.plot(date_list, result_list, 'bo') 
plt.show()
 
plt.figure(2)

plt.subplot(212)
plt.title("pH level graph")
plt.bar(date_list, ph_list)

plt.show()

